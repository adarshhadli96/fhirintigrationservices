﻿using YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models;

namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.DataSource
{
    public class TrialData
    {
        //private static MessageQueue messagequeues = new MessageQueue();
        private static List<Patient> patients = new List<Patient>();
        private static List<Address> addresses = new List<Address>();

        public static MessageQueue GetMessageQueues() 
        {
            MessageQueue messageQueueobj1 = MessageQueue.Create("", "2022-05-17", "I", "", patients, "", "Patient", "Medisoft", "");


            return messageQueueobj1; 
        }

        public static List<Patient> GetPatients()
        {
            Patient patientobj1 = Patient.Create("MEDINT0005", "Mr", "ABC1", "XYZ1", "1991-02-15", "North Carolina", "US", "Male", addresses);
            Patient patientobj2 = Patient.Create("MEDINT0006", "Mrs", "XYZ2", "ABC2", "1991-02-15", "North Carolina", "US", "Female", addresses);
            
            patients.Add(patientobj1);
            patients.Add(patientobj2);
            
            return patients;
        }

        public static List<Address> GetAddresses()
        {
            Address addressobj1 = Address.Create("St1","18", "1008", "27703", "North Corolina", "US", "string");
           
            addresses.Add(addressobj1);
            
            return addresses;
        }
    }
}
