﻿using Newtonsoft.Json;


namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models
{
    public class MessageQueue
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("createdDate")]
        public string CreatedDate { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("processingDate")]
        public string ProcessingDate { get; set; }

        [JsonProperty("payload")]
        public List<Patient> Payload { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("resourceType")]
        public string ResourceType { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
        

        public static MessageQueue Create(string id, string createdDate, string type, string processingDate, List<Patient> payload, string error, string resourceType, string source, string status)
        {
            MessageQueue messageQueue = new MessageQueue
            {
                Id = id,
                CreatedDate = createdDate,
                Type = type,
                ProcessingDate = processingDate,
                Payload = payload,
                Error = error,
                ResourceType = resourceType,
                Source = source,
                Status = status,

            };
            return messageQueue;
        }
    }
}