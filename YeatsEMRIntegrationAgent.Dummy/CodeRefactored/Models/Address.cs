﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models
{
    public class Address
    {
        [JsonProperty("streetName")]
        public string StreetName { get; set; }

        [JsonProperty("streetNo")]
        public string StreetNo { get; set; }

        [JsonProperty("appartmentNo")]
        public string AppartmentNo { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        public static Address Create(string streetName, string streetNo, string appartmentNo, string postalCode, string city, string country, string type)
        {
            Address address=  new Address
            {
                StreetName = streetName,
                StreetNo = streetNo,
                AppartmentNo = appartmentNo,
                PostalCode = postalCode,
                City = city,
                Country = country,
                Type = type
            };
            return address;
        }

    }
}
