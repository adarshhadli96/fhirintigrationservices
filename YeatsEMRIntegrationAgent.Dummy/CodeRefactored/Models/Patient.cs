﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models
{
    public class Patient
    {
        [JsonProperty("identifier")]
        public string Identifier { get; set; }

        [JsonProperty("prefix")]
        public string Prefix { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("birthDate")]
        public string BirthDate { get; set; }

        [JsonProperty("birthPlace")]
        public string BirthPlace { get; set; }

        [JsonProperty("citizenshipCode")]
        public string CitizenshipCode { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("address")]
        public List<Address> Address { get; set; }

        public static Patient Create(string identifier, string prefix, string firstname, string lastname, string birthdate, string birthplace, string citizenshipcode, string gender, List<Address> addresses)
        {
            Patient patient = new Patient
            {
                Identifier = identifier,
                Prefix = prefix,
                FirstName = firstname,
                LastName = lastname,
                BirthDate = birthdate,
                BirthPlace = birthplace,
                CitizenshipCode = citizenshipcode,
                Gender = gender,
                Address = addresses
            };
            return patient;
        }

    }
}
