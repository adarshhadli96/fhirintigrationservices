﻿using YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Interface;
using YeatsEMRIntegrationAgent.Infrastructure.Models;
using MessageQueue = YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models.MessageQueue;

namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Services
{
    public class MessageQueueRepository : IMessageQueueRepository
    {
        public bool Create(MessageQueue mq)
        {
            using (IntegrationsContext dbContext = new IntegrationsContext())
            {
                var pushingMessageQueue = mq;
                dbContext.Add(pushingMessageQueue);
                dbContext.SaveChanges();
                return true;
            }
        }

        public List<MessageQueue> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
