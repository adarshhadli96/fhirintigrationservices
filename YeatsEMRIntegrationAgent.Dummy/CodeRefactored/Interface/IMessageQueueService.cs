﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models;

namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Interface
{
    public interface IMessageQueueService
    {
        public void DisplayMessageQueuedetails(MessageQueue mq);
        public void DisplayMessageQueuesdetails(List<MessageQueue> mqs);
        public List<MessageQueue> GetMessageQueuesdetails();
       
    }
}
