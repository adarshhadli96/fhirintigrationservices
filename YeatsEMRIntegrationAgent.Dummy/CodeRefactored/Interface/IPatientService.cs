﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models;

namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Interface
{
    public interface IPatientService
    {
        public void DisplayPatientdetails(Patient patient);
        public void DisplayPatientsdetails(List<Patient> patients);
        public List<Patient> GetPatientsdetails();
    }
}
