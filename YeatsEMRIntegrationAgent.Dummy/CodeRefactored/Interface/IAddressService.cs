﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models;

namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Interface
{
    public interface IAddressService
    {
        public void DisplayAddressdetails(Address mq);
        public void DisplayAddressdetails(List<Address> mqs);
        public List<Address> GetAddressdetails();
    }
}
