﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Models;

namespace YeatsEMRIntegrationAgent.Dummy.CodeRefactored.Interface
{
    public interface IMessageQueueRepository
    {
        public bool Create(MessageQueue mq);
        public List<MessageQueue> GetAll();
    }
}
