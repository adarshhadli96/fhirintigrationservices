﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Infrastructure.DTO
{
    public class FacilityDto
    {
        public string FacilityId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string AltPhone { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

    }
}
