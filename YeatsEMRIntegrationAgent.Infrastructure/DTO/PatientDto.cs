﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Infrastructure.DTO
{
    public class PatientDto
    {
        public string PatientId { get; set; }
        public string Identifier { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string CitizenshipCode { get; set; }
        public string Gender { get; set; }
        public PatientAddressDto Address { get; set; }
        public string Suffix { get; set; }
        public string Race { get; set; }
        public string MiddleName { get; set; }
        public string Ethnicity { get; set; }
        public string Language { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string WPhone { get; set; }

        public string HPhone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string AltPhone { get; set; }
        public string Ssn { get; set; }
        public string DefaultPhysician { get; set; }
        public string Email { get; set; }
        public string MarritalStatus { get; set; }
        public string Inactive { get; set; }
        public PhysicianDto Physician { get; set; }
        public FacilityDto Facility { get; set; }
        public PhysicianDto RefferingPhysician { get; set; }
        public PatientDto Gurantor { get; set; }

    }
}
