﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Infrastructure.DTO;

namespace YeatsEMRIntegrationAgent.Infrastructure.DTO
{

    public class AppointmentDto
    {
        public int Identifier { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string Start_time { get; set; }
        public string End_time { get; set; }
        public string Length { get; set; }
        public string Provider { get; set; }
        public string Chart_number { get; set; }
        public string Case_number { get; set; }
        public string Status { get; set; }
        public string Duration { get; set; }
        public string Check_out_time { get; set; }
        public string Check_in_time { get; set; }
        public string Type { get; set; }
        public string Note { get; set; }
        public string Priority { get; set; }
        public string CancellationReason { get; set; }
        public string PatientInstructions { get; set; }
        public PhysicianDto Physician { get; set; }
        public PhysicianDto ReferringPhysician { get; set; }
        public FacilityDto Facility { get; set; }
        public PatientDto Patient { get; set; }



    }
}
