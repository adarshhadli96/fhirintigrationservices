﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Infrastructure.Mappers
{
    public class MapperCoreToIntegration
    {
        public static YeatsEMRIntegrationAgent.Infrastructure.Models.MessageQueue MapCoreToIntegration(YeatsEMRIntegrationAgent.Core.Entities.MessageQueue mapmessageQueue)
        {
            var message = new YeatsEMRIntegrationAgent.Infrastructure.Models.MessageQueue
            {
                Id = long.Parse(mapmessageQueue.Id),
                CreatedDate = Convert.ToDateTime(mapmessageQueue.CreatedDate),
                Type = mapmessageQueue.Type,
                ProcessingDate = Convert.ToDateTime(mapmessageQueue.ProcessingDate),
                Payload = mapmessageQueue.Payload,
                Error = mapmessageQueue.Error,
                ResourceType = mapmessageQueue.ResourceType,
                Source = mapmessageQueue.Source,
                Status = mapmessageQueue.Status
            };
            return message;
        }

        public static List<YeatsEMRIntegrationAgent.Core.Entities.MessageQueue> MapCoreToIntegration(List<YeatsEMRIntegrationAgent.Infrastructure.Models.MessageQueue> mapmessageQueues)
        {
            var listmessages = mapmessageQueues.Select(x => new YeatsEMRIntegrationAgent.Core.Entities.MessageQueue
            {
                Id = Convert.ToString(x.Id),
                CreatedDate = Convert.ToString(x.CreatedDate),
                Type = x.Type,
                ProcessingDate = Convert.ToString(x.ProcessingDate),
                Payload = x.Payload,
                Error = x.Error,
                ResourceType = x.ResourceType,
                Source = x.Source,
                Status = x.Status
            }).ToList();
            return listmessages;
        }
    }
}
