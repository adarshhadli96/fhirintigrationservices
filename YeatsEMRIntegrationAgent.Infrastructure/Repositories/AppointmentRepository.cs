﻿using YeatsEMRIntegrationAgent.Infrastructure.Interfaces;
using YeatsEMRIntegrationAgent.Infrastructure.Models;

namespace YeatsEMRIntegrationAgent.Infrastructure.Repositories
{
    public class AppointmentRepository : IAppointment
    {
        private HistestContext _db;
        public List<Appointment> GetAll()
        {
            return _db.Appointments.ToList();
        }

        public bool insert(Appointment appointment)
        {
            _db.Appointments.Add(appointment);
            _db.SaveChanges();
            return true;
        }

        public bool update(Appointment appointment)
        {
            _db.Appointments.Update(appointment);
            _db.SaveChanges();
            return true;
        }

        public bool delete(int id)
        {
            var _appointment = _db.Appointments.Where(x => x.AppointmentId == id).FirstOrDefault();
            _db.Appointments.Remove(_appointment);
            _db.SaveChanges();
            return true;
        }
    }
}
