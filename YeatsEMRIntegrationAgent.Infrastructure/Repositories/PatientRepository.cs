﻿using YeatsEMRIntegrationAgent.Infrastructure.Models;
//using YeatsEMRIntegrationAgent.Infrastructure.Context;
using YeatsEMRIntegrationAgent.Infrastructure.Interfaces;
//using Patient = YeatsEMRIntegrationAgent.Infrastructure.Models.Patient;

namespace YeatsEMRIntegrationAgent.Infrastructure.Repositories
{
    public class PatientRepository : IPatient
    {
        private HistestContext _db;
        public List<Patient> GetAll()
        {
            return _db.Patients.ToList();
        }

        public bool insert(Patient patient)
        {
            _db.Patients.Add(patient);
            _db.SaveChanges();
            return true;
        }

        public bool update(Patient patient)
        {
            _db.Patients.Update(patient);
            return true;
        }

        public bool delete(int id)
        {
            var _patient = _db.Patients.Where(x => x.PatientId == id).FirstOrDefault();
            _db.Patients.Remove(_patient);
            _db.SaveChanges();
            return true;
        }
    }
}
