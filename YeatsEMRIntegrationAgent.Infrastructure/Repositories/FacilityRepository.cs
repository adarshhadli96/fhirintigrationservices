﻿//using YeatsEMRIntegrationAgent.Infrastructure.Context;
using YeatsEMRIntegrationAgent.Infrastructure.Models;
using YeatsEMRIntegrationAgent.Infrastructure.Interfaces;
//using Facility = YeatsEMRIntegrationAgent.Infrastructure.Models.Facility;

namespace YeatsEMRIntegrationAgent.Infrastructure.Repositories
{
    public class FacilityRepository : IFacility
    {
        private HistestContext _db;
        public List<Facility> GetAll()
        {
            return _db.Facilities.ToList();
        }

        public bool insert(Facility facility)
        {
            _db.Facilities.Add(facility);
            _db.SaveChanges();
            return true;
        }

        public bool update(Facility facility)
        {
            _db.Facilities.Update(facility);
            _db.SaveChanges();
            return true;
        }

        public bool delete(int id)
        {
            var _facility = _db.Facilities.Where(x => x.FacilityId == id).FirstOrDefault();
            _db.Facilities.Remove(_facility);
            _db.SaveChanges();
            return true;
        }
    }
}
