﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Infrastructure.DTO;
using YeatsEMRIntegrationAgent.Infrastructure.Interfaces;
using YeatsEMRIntegrationAgent.Infrastructure.Mappers;
using YeatsEMRIntegrationAgent.Infrastructure.Models;

namespace YeatsEMRIntegrationAgent.Infrastructure.Repositories
{
    public class MessageQueueRepository : IMessageQueue
    {
        public List<Core.Entities.MessageQueue> GetAll()
        {
            using (IntegrationsContext dbContext = new IntegrationsContext())
            {
                return MapperCoreToIntegration.MapCoreToIntegration(dbContext.MessageQueues.ToList());
            }
            
        }

        public bool insert(Core.Entities.MessageQueue imq)
        {
            using (IntegrationsContext dbContext = new IntegrationsContext())
            {
                var mappedMessageQueue = MapperCoreToIntegration.MapCoreToIntegration(imq);
                dbContext.MessageQueues.Add(mappedMessageQueue);
                dbContext.SaveChanges();
                return true;
            }
        }
    }
}
