﻿using System;
using System.Collections.Generic;

namespace YeatsEMRIntegrationAgent.Infrastructure.Models;

public partial class Facility
{
    public int FacilityId { get; set; }

    public string FacilityName { get; set; } = null!;

    public int PracticeId { get; set; }

    public string AddressLine1 { get; set; } = null!;

    public string? AddressLine2 { get; set; }

    public string City { get; set; } = null!;

    public string StateCode { get; set; } = null!;

    public string ZipCode { get; set; } = null!;

    public string? ZipPlus4 { get; set; }

    public string? MainPhone { get; set; }

    public string? AltPhone { get; set; }

    public string? Fax { get; set; }

    public byte[]? LogoImage { get; set; }

    public string? Email { get; set; }

    public string? Website { get; set; }

    public string? TaxIdentificationNumber { get; set; }

    public int PlaceOfServiceCode { get; set; }

    public string? X12SenderId { get; set; }

    public string? FacilityNpi { get; set; }

    public bool? Inactive { get; set; }

    public bool WorkOnScheduledHolidays { get; set; }

    public DateTime DateCreated { get; set; }

    public int CreatedByUserId { get; set; }

    public DateTime DateLastUpdated { get; set; }

    public int LastUpdatedByUserId { get; set; }

    public virtual ICollection<Patient> Patients { get; } = new List<Patient>();
}
