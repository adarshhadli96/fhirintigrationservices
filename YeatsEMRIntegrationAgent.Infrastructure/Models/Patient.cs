﻿using System;
using System.Collections.Generic;

namespace YeatsEMRIntegrationAgent.Infrastructure.Models;

public partial class Patient
{
    public int PatientId { get; set; }

    public string? ChartNumber { get; set; }

    public string? Ssn { get; set; }

    public DateTime? DateOfBirth { get; set; }

    public int? SalutationId { get; set; }

    public int? MaritalStatusId { get; set; }

    public bool Sex { get; set; }

    public int? ReligionId { get; set; }

    public int? RaceId { get; set; }

    public int? EthnicityId { get; set; }

    public int? Language1Id { get; set; }

    public int? Language2Id { get; set; }

    public int? Language3Id { get; set; }

    public bool? RequiresInterpreter { get; set; }

    public string? FirstName { get; set; }

    public string? MiddleName { get; set; }

    public string? LastName { get; set; }

    public string? PrimaryAddressLine1 { get; set; }

    public string? PrimaryAddressLine2 { get; set; }

    public string? PrimaryCity { get; set; }

    public string? PrimaryStateCode { get; set; }

    public string? PrimaryZipCode { get; set; }

    public string? PrimaryZipPlus4 { get; set; }

    public string? SecondaryAddressLine1 { get; set; }

    public string? SecondaryAddressLine2 { get; set; }

    public string? SecondaryCity { get; set; }

    public string? SecondaryStateCode { get; set; }

    public string? SecondaryZipCode { get; set; }

    public string? SecondaryZipPlus4 { get; set; }

    public bool? HasInternationalAddress { get; set; }

    public string? WorkPhone { get; set; }

    public string? WorkPhoneExt { get; set; }

    public string? HomePhone { get; set; }

    public string? MobilePhone { get; set; }

    public string? AltPhone { get; set; }

    public string? Fax { get; set; }

    public short? PreferredContactNo { get; set; }

    public byte[]? PhotoImage { get; set; }

    public string? Email1 { get; set; }

    public string? Email2 { get; set; }

    public string? GeneralNote { get; set; }

    public string? Occupation { get; set; }

    public bool HipaaViaMail { get; set; }

    public bool HipaaViaVoice { get; set; }

    public bool HipaaViaNotice { get; set; }

    public bool HipaaViaMessage { get; set; }

    public bool HipaaAllowSms { get; set; }

    public bool HipaaAllowEmail { get; set; }

    public string? PriceLevel { get; set; }

    public string? PatientStatus { get; set; }

    public int? ReferralSourceId { get; set; }

    public DateTime DateCreated { get; set; }

    public int CreatedByUserId { get; set; }

    public DateTime DateLastUpdated { get; set; }

    public int LastUpdatedByUserId { get; set; }

    public bool SignatureOnFile { get; set; }

    public int? AssignedProvider { get; set; }

    public DateTime? SofDate { get; set; }

    public string? MedicalChartNumber { get; set; }

    public int? DefaultFacility { get; set; }

    public int? DefaultPhysician { get; set; }

    public int? UserId { get; set; }

    public int? ApplicationUserId { get; set; }

    public bool HippaAllowPhoneCall { get; set; }

    public bool HippaAllowPatientPortal { get; set; }

    public bool? PatientPortalMakeClinicalSummaryAvailableToPatient { get; set; }

    public bool? PatientPortalMakeLabResultsAvailableToPatient { get; set; }

    public bool? PatientPortalMakeProblemListAvailableToPatient { get; set; }

    public bool? PatientPortalMakeMedicationListAvailableToPatient { get; set; }

    public bool? PatientPortalMakeAllergyInfoAvailableToPatient { get; set; }

    public bool? PatientPortalMakeRadiologyReportsInfoAvailableToPatient { get; set; }

    public bool? PatientPortalMakeReferralLettersInfoAvailableToPatient { get; set; }

    public int? ReferringPhysicianId { get; set; }

    public int? ReferringOrganizationId { get; set; }

    public string? ReferralDetails { get; set; }

    public byte[]? SummaryDocument { get; set; }

    public decimal? AmountCurrentBalanceDue { get; set; }

    public string? AmountCurrentBalanceDueCurrency { get; set; }

    public DateTime? DateCurrentBalanceDue { get; set; }

    public bool? OnPaymentArrangement { get; set; }

    public string? UniqueNumber { get; set; }

    public bool? IsLivingWill { get; set; }

    public int? AdvancedDirectivesId { get; set; }

    public string? Note { get; set; }

    public bool? FlagSelfPayPatient { get; set; }

    public bool? FlagDoNotSendPatientStatement { get; set; }

    public int? InsuranceCategoryId { get; set; }

    public bool? AcceptAssignment { get; set; }

    public string? RcopiaPatientId { get; set; }

    public string? RcopiaXml { get; set; }

    public bool? IsPregnant { get; set; }

    public bool? IsBreastfeeding { get; set; }

    public int? PatientAdmissionId { get; set; }

    public virtual ICollection<Appointment> Appointments { get; } = new List<Appointment>();

    public virtual Facility? DefaultFacilityNavigation { get; set; }
}
