﻿using System;
using System.Collections.Generic;

namespace YeatsEMRIntegrationAgent.Infrastructure.Models;

public partial class Appointment
{
    public int AppointmentId { get; set; }

    public int PatientId { get; set; }

    public int VisitId { get; set; }

    public string? AppointmentStatus { get; set; }

    public int? AppointmentReasonId { get; set; }

    public int? AppointmentTypeId { get; set; }

    public bool? SendReminder { get; set; }

    public DateTime? ReminderDate { get; set; }

    public bool? SmsReminderSent { get; set; }

    public bool? EmailReminderSent { get; set; }

    public bool? PhoneReminderMade { get; set; }

    public int? PhoneReminderByUserId { get; set; }

    public string? Comments { get; set; }

    public DateTime? CheckInTime { get; set; }

    public DateTime DateCreated { get; set; }

    public int CreatedByUserId { get; set; }

    public DateTime DateLastUpdated { get; set; }

    public int LastUpdatedByUserId { get; set; }

    public string? ExceptionAppointments { get; set; }

    public decimal? CoPayAmount { get; set; }

    public string? PaymentMethod { get; set; }

    public string? CheckNumber { get; set; }

    public decimal? InsuranceCopayRequired { get; set; }

    public bool? IsCopayPercentage { get; set; }

    public bool? Confirmed { get; set; }

    public string? ConfirmedStatus { get; set; }

    public int? ConfirmedByUserId { get; set; }

    public DateTime? DateConfirmed { get; set; }

    public string? CancelledStatus { get; set; }

    public int? CancelledByUserId { get; set; }

    public DateTime? DateCancelled { get; set; }

    public int? ReferringPhysicianId { get; set; }

    public int? InternalReferringPhysicianId { get; set; }

    public bool IsSpecialistVisit { get; set; }

    public bool? FlagSelfPayPatient { get; set; }

    public decimal? Deductible { get; set; }

    public decimal? CoInsurancePatientAmount { get; set; }

    public decimal? CoInsurancePatientPercentage { get; set; }

    public bool IsWalkIn { get; set; }

    public decimal? InsuranceCoInsuranceRequired { get; set; }

    public decimal? InsuranceDeductibleRequired { get; set; }

    public string? PriorAuthorizationNumber { get; set; }

    public int? AllowedVisits { get; set; }

    public int? VisitsUsed { get; set; }

    public DateTime? ReferralStartDate { get; set; }

    public DateTime? ReferralExpiryDate { get; set; }

    public string? ExternalReferenceId { get; set; }

    public bool? DoNotBillAppointment { get; set; }

    public int? CaseNumber { get; set; }

    public int? RoomId { get; set; }

    public bool? IsTelemed { get; set; }

    public Guid? TelemedVerifyCode { get; set; }

    public virtual Patient Patient { get; set; } = null!;
}
