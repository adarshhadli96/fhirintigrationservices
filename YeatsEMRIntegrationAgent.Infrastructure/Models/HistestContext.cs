﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace YeatsEMRIntegrationAgent.Infrastructure.Models;

public partial class HistestContext : DbContext
{
    public HistestContext()
    {
    }

    public HistestContext(DbContextOptions<HistestContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Appointment> Appointments { get; set; }

    public virtual DbSet<Facility> Facilities { get; set; }

    public virtual DbSet<Patient> Patients { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=174.73.51.169\\\\\\\\SQLEXPRESS,6099;Database=histest;Trusted_Connection=False;TrustServerCertificate=True;User ID=sa;Password=Matrics@1234;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.UseCollation("SQL_Latin1_General_CP1_CI_AS");

        modelBuilder.Entity<Appointment>(entity =>
        {
            entity.ToTable("appointment", tb => tb.HasTrigger("trigger_HL7Integrations"));

            entity.HasIndex(e => e.VisitId, "nci_wi_appointment_1329CFD344AB2A770CEBB5E99371F724");

            entity.HasIndex(e => e.ExternalReferenceId, "nci_wi_appointment_54B98314CD1D20D4A0F085C0E6EC1FB0");

            entity.HasIndex(e => e.AppointmentStatus, "nci_wi_appointment_FCD8C99D7E893C1C794DBCD97EAF34C7");

            entity.Property(e => e.AppointmentId).HasColumnName("appointment_id");
            entity.Property(e => e.AllowedVisits).HasColumnName("allowed_visits");
            entity.Property(e => e.AppointmentReasonId)
                .HasDefaultValueSql("((1))")
                .HasColumnName("appointment_reason_id");
            entity.Property(e => e.AppointmentStatus)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasColumnName("appointment_status");
            entity.Property(e => e.AppointmentTypeId)
                .HasDefaultValueSql("((1))")
                .HasColumnName("appointment_type_id");
            entity.Property(e => e.CancelledByUserId).HasColumnName("cancelled_by_user_id");
            entity.Property(e => e.CancelledStatus)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasColumnName("cancelled_status");
            entity.Property(e => e.CaseNumber).HasColumnName("case_number");
            entity.Property(e => e.CheckInTime)
                .HasColumnType("datetime")
                .HasColumnName("check_in_time");
            entity.Property(e => e.CheckNumber)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("check_number");
            entity.Property(e => e.CoInsurancePatientAmount)
                .HasColumnType("money")
                .HasColumnName("co_insurance_patient_amount");
            entity.Property(e => e.CoInsurancePatientPercentage)
                .HasColumnType("decimal(18, 0)")
                .HasColumnName("co_insurance_patient_percentage");
            entity.Property(e => e.CoPayAmount)
                .HasColumnType("money")
                .HasColumnName("co_pay_amount");
            entity.Property(e => e.Comments)
                .HasMaxLength(2000)
                .IsUnicode(false)
                .HasColumnName("comments");
            entity.Property(e => e.Confirmed).HasColumnName("confirmed");
            entity.Property(e => e.ConfirmedByUserId).HasColumnName("confirmed_by_user_id");
            entity.Property(e => e.ConfirmedStatus)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasColumnName("confirmed_status");
            entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");
            entity.Property(e => e.DateCancelled)
                .HasColumnType("datetime")
                .HasColumnName("date_cancelled");
            entity.Property(e => e.DateConfirmed)
                .HasColumnType("datetime")
                .HasColumnName("date_confirmed");
            entity.Property(e => e.DateCreated)
                .HasColumnType("datetime")
                .HasColumnName("date_created");
            entity.Property(e => e.DateLastUpdated)
                .HasColumnType("datetime")
                .HasColumnName("date_last_updated");
            entity.Property(e => e.Deductible)
                .HasColumnType("money")
                .HasColumnName("deductible");
            entity.Property(e => e.DoNotBillAppointment).HasColumnName("do_not_bill_appointment");
            entity.Property(e => e.EmailReminderSent).HasColumnName("email_reminder_sent");
            entity.Property(e => e.ExceptionAppointments)
                .HasMaxLength(4000)
                .HasColumnName("exception_appointments");
            entity.Property(e => e.ExternalReferenceId)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("external_reference_id");
            entity.Property(e => e.FlagSelfPayPatient).HasColumnName("flag_self_pay_patient");
            entity.Property(e => e.InsuranceCoInsuranceRequired)
                .HasColumnType("money")
                .HasColumnName("insurance_co_insurance_required");
            entity.Property(e => e.InsuranceCopayRequired)
                .HasColumnType("money")
                .HasColumnName("insurance_copay_required");
            entity.Property(e => e.InsuranceDeductibleRequired)
                .HasColumnType("money")
                .HasColumnName("insurance_deductible_required");
            entity.Property(e => e.InternalReferringPhysicianId).HasColumnName("internal_referring_physician_id");
            entity.Property(e => e.IsCopayPercentage).HasColumnName("is_copay_percentage");
            entity.Property(e => e.IsSpecialistVisit).HasColumnName("is_specialist_visit");
            entity.Property(e => e.IsTelemed).HasColumnName("is_telemed");
            entity.Property(e => e.IsWalkIn).HasColumnName("is_walk_in");
            entity.Property(e => e.LastUpdatedByUserId).HasColumnName("last_updated_by_user_id");
            entity.Property(e => e.PatientId).HasColumnName("patient_id");
            entity.Property(e => e.PaymentMethod)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("payment_method");
            entity.Property(e => e.PhoneReminderByUserId).HasColumnName("phone_reminder_by_user_id");
            entity.Property(e => e.PhoneReminderMade).HasColumnName("phone_reminder_made");
            entity.Property(e => e.PriorAuthorizationNumber)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("prior_authorization_number");
            entity.Property(e => e.ReferralExpiryDate)
                .HasColumnType("datetime")
                .HasColumnName("referral_expiry_date");
            entity.Property(e => e.ReferralStartDate)
                .HasColumnType("datetime")
                .HasColumnName("referral_start_date");
            entity.Property(e => e.ReferringPhysicianId).HasColumnName("referring_physician_id");
            entity.Property(e => e.ReminderDate)
                .HasColumnType("datetime")
                .HasColumnName("reminder_date");
            entity.Property(e => e.RoomId).HasColumnName("room_id");
            entity.Property(e => e.SendReminder).HasColumnName("send_reminder");
            entity.Property(e => e.SmsReminderSent).HasColumnName("sms_reminder_sent");
            entity.Property(e => e.TelemedVerifyCode).HasColumnName("telemed_verify_code");
            entity.Property(e => e.VisitId).HasColumnName("visit_id");
            entity.Property(e => e.VisitsUsed).HasColumnName("visits_used");

            entity.HasOne(d => d.Patient).WithMany(p => p.Appointments)
                .HasForeignKey(d => d.PatientId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_appointment_patient");
        });

        modelBuilder.Entity<Facility>(entity =>
        {
            entity.HasKey(e => e.FacilityId).HasName("facility_pk");

            entity.ToTable("facility", tb => tb.HasTrigger("trigger_MFN_01_Facility"));

            entity.Property(e => e.FacilityId).HasColumnName("facility_id");
            entity.Property(e => e.AddressLine1)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("address_line_1");
            entity.Property(e => e.AddressLine2)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("address_line_2");
            entity.Property(e => e.AltPhone)
                .HasMaxLength(14)
                .IsUnicode(false)
                .HasColumnName("alt_phone");
            entity.Property(e => e.City)
                .HasMaxLength(25)
                .IsUnicode(false);
            entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");
            entity.Property(e => e.DateCreated)
                .HasColumnType("datetime")
                .HasColumnName("date_created");
            entity.Property(e => e.DateLastUpdated)
                .HasColumnType("datetime")
                .HasColumnName("date_last_updated");
            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("email");
            entity.Property(e => e.FacilityName)
                .HasMaxLength(500)
                .IsUnicode(false)
                .HasColumnName("facility_name");
            entity.Property(e => e.FacilityNpi)
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasColumnName("facility_npi");
            entity.Property(e => e.Fax)
                .HasMaxLength(14)
                .IsUnicode(false);
            entity.Property(e => e.Inactive).HasColumnName("inactive");
            entity.Property(e => e.LastUpdatedByUserId).HasColumnName("last_updated_by_user_id");
            entity.Property(e => e.LogoImage)
                .HasColumnType("image")
                .HasColumnName("logo_image");
            entity.Property(e => e.MainPhone)
                .HasMaxLength(14)
                .IsUnicode(false)
                .HasColumnName("main_phone");
            entity.Property(e => e.PlaceOfServiceCode).HasColumnName("place_of_service_code");
            entity.Property(e => e.PracticeId).HasColumnName("practice_id");
            entity.Property(e => e.StateCode)
                .HasMaxLength(2)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("state_code");
            entity.Property(e => e.TaxIdentificationNumber)
                .HasMaxLength(14)
                .IsUnicode(false)
                .HasColumnName("tax_identification_number");
            entity.Property(e => e.Website)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasColumnName("website");
            entity.Property(e => e.WorkOnScheduledHolidays).HasColumnName("work_on_scheduled_holidays");
            entity.Property(e => e.X12SenderId)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("x12_sender_id");
            entity.Property(e => e.ZipCode)
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasColumnName("zip_code");
            entity.Property(e => e.ZipPlus4)
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasColumnName("zip_plus_4");
        });

        modelBuilder.Entity<Patient>(entity =>
        {
            entity.ToTable("patient", tb =>
                {
                    tb.HasTrigger("trigger_ADT");
                    tb.HasTrigger("trigger_audit_patient");
                });

            entity.Property(e => e.PatientId).HasColumnName("patient_id");
            entity.Property(e => e.AcceptAssignment).HasColumnName("accept_assignment");
            entity.Property(e => e.AdvancedDirectivesId).HasColumnName("Advanced_Directives_Id");
            entity.Property(e => e.AltPhone)
                .HasMaxLength(14)
                .HasColumnName("alt_phone");
            entity.Property(e => e.AmountCurrentBalanceDue)
                .HasColumnType("money")
                .HasColumnName("amount_current_balance_due");
            entity.Property(e => e.AmountCurrentBalanceDueCurrency)
                .HasMaxLength(5)
                .HasColumnName("amount_current_balance_due_currency");
            entity.Property(e => e.ApplicationUserId).HasColumnName("application_user_id");
            entity.Property(e => e.AssignedProvider).HasColumnName("assigned_provider");
            entity.Property(e => e.ChartNumber)
                .HasMaxLength(50)
                .HasColumnName("chart_number");
            entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");
            entity.Property(e => e.DateCreated)
                .HasColumnType("datetime")
                .HasColumnName("date_created");
            entity.Property(e => e.DateCurrentBalanceDue)
                .HasColumnType("date")
                .HasColumnName("date_current_balance_due");
            entity.Property(e => e.DateLastUpdated)
                .HasColumnType("datetime")
                .HasColumnName("date_last_updated");
            entity.Property(e => e.DateOfBirth)
                .HasColumnType("datetime")
                .HasColumnName("date_of_birth");
            entity.Property(e => e.DefaultFacility).HasColumnName("default_facility");
            entity.Property(e => e.DefaultPhysician).HasColumnName("default_physician");
            entity.Property(e => e.Email1)
                .HasMaxLength(100)
                .HasColumnName("email1");
            entity.Property(e => e.Email2)
                .HasMaxLength(100)
                .HasColumnName("email2");
            entity.Property(e => e.EthnicityId).HasColumnName("ethnicity_id");
            entity.Property(e => e.Fax)
                .HasMaxLength(14)
                .HasColumnName("fax");
            entity.Property(e => e.FirstName)
                .HasMaxLength(255)
                .HasColumnName("first_name");
            entity.Property(e => e.FlagDoNotSendPatientStatement).HasColumnName("flag_do_not_send_patient_statement");
            entity.Property(e => e.FlagSelfPayPatient).HasColumnName("flag_self_pay_patient");
            entity.Property(e => e.GeneralNote).HasColumnName("general_note");
            entity.Property(e => e.HasInternationalAddress).HasColumnName("has_international_address");
            entity.Property(e => e.HipaaAllowEmail).HasColumnName("hipaa_allow_email");
            entity.Property(e => e.HipaaAllowSms).HasColumnName("hipaa_allow_sms");
            entity.Property(e => e.HipaaViaMail).HasColumnName("hipaa_via_mail");
            entity.Property(e => e.HipaaViaMessage).HasColumnName("hipaa_via_message");
            entity.Property(e => e.HipaaViaNotice).HasColumnName("hipaa_via_notice");
            entity.Property(e => e.HipaaViaVoice).HasColumnName("hipaa_via_voice");
            entity.Property(e => e.HippaAllowPatientPortal).HasColumnName("hippa_allow_patient_portal");
            entity.Property(e => e.HippaAllowPhoneCall).HasColumnName("hippa_allow_phone_call");
            entity.Property(e => e.HomePhone)
                .HasMaxLength(14)
                .HasColumnName("home_phone");
            entity.Property(e => e.InsuranceCategoryId).HasColumnName("insurance_category_id");
            entity.Property(e => e.IsBreastfeeding).HasColumnName("is_breastfeeding");
            entity.Property(e => e.IsLivingWill).HasColumnName("Is_Living_Will");
            entity.Property(e => e.IsPregnant).HasColumnName("is_pregnant");
            entity.Property(e => e.Language1Id).HasColumnName("language_1_id");
            entity.Property(e => e.Language2Id).HasColumnName("language_2_id");
            entity.Property(e => e.Language3Id).HasColumnName("language_3_id");
            entity.Property(e => e.LastName)
                .HasMaxLength(255)
                .HasColumnName("last_name");
            entity.Property(e => e.LastUpdatedByUserId).HasColumnName("last_updated_by_user_id");
            entity.Property(e => e.MaritalStatusId).HasColumnName("marital_status_id");
            entity.Property(e => e.MedicalChartNumber)
                .HasMaxLength(50)
                .HasColumnName("medical_chart_number");
            entity.Property(e => e.MiddleName)
                .HasMaxLength(255)
                .HasColumnName("middle_name");
            entity.Property(e => e.MobilePhone)
                .HasMaxLength(14)
                .HasColumnName("mobile_phone");
            entity.Property(e => e.Note).HasMaxLength(255);
            entity.Property(e => e.Occupation)
                .HasMaxLength(250)
                .HasColumnName("occupation");
            entity.Property(e => e.OnPaymentArrangement)
                .HasDefaultValueSql("((0))")
                .HasColumnName("on_payment_arrangement");
            entity.Property(e => e.PatientAdmissionId).HasColumnName("patient_admission_id");
            entity.Property(e => e.PatientPortalMakeAllergyInfoAvailableToPatient)
                .IsRequired()
                .HasDefaultValueSql("((1))")
                .HasColumnName("patient_portal_make_allergy_info_available_to_patient");
            entity.Property(e => e.PatientPortalMakeClinicalSummaryAvailableToPatient)
                .IsRequired()
                .HasDefaultValueSql("((1))")
                .HasColumnName("patient_portal_make_clinical_summary_available_to_patient");
            entity.Property(e => e.PatientPortalMakeLabResultsAvailableToPatient)
                .IsRequired()
                .HasDefaultValueSql("((1))")
                .HasColumnName("patient_portal_make_lab_results_available_to_patient");
            entity.Property(e => e.PatientPortalMakeMedicationListAvailableToPatient)
                .IsRequired()
                .HasDefaultValueSql("((1))")
                .HasColumnName("patient_portal_make_medication_list_available_to_patient");
            entity.Property(e => e.PatientPortalMakeProblemListAvailableToPatient)
                .IsRequired()
                .HasDefaultValueSql("((1))")
                .HasColumnName("patient_portal_make_problem_list_available_to_patient");
            entity.Property(e => e.PatientPortalMakeRadiologyReportsInfoAvailableToPatient)
                .IsRequired()
                .HasDefaultValueSql("((1))")
                .HasColumnName("patient_portal_make_radiology_reports_info_available_to_patient");
            entity.Property(e => e.PatientPortalMakeReferralLettersInfoAvailableToPatient)
                .IsRequired()
                .HasDefaultValueSql("((1))")
                .HasColumnName("patient_portal_make_referral_letters_info_available_to_patient");
            entity.Property(e => e.PatientStatus)
                .HasMaxLength(50)
                .HasColumnName("patient_status");
            entity.Property(e => e.PhotoImage)
                .HasColumnType("image")
                .HasColumnName("photo_image");
            entity.Property(e => e.PreferredContactNo).HasColumnName("preferred_contact_no");
            entity.Property(e => e.PriceLevel)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasDefaultValueSql("('standard')")
                .HasColumnName("price_level");
            entity.Property(e => e.PrimaryAddressLine1)
                .HasMaxLength(100)
                .HasColumnName("primary_address_line_1");
            entity.Property(e => e.PrimaryAddressLine2)
                .HasMaxLength(100)
                .HasColumnName("primary_address_line_2");
            entity.Property(e => e.PrimaryCity)
                .HasMaxLength(25)
                .HasColumnName("primary_city");
            entity.Property(e => e.PrimaryStateCode)
                .HasMaxLength(2)
                .HasColumnName("primary_state_code");
            entity.Property(e => e.PrimaryZipCode)
                .HasMaxLength(5)
                .HasColumnName("primary_zip_code");
            entity.Property(e => e.PrimaryZipPlus4)
                .HasMaxLength(4)
                .HasColumnName("primary_zip_plus_4");
            entity.Property(e => e.RaceId).HasColumnName("race_id");
            entity.Property(e => e.RcopiaPatientId)
                .HasMaxLength(255)
                .HasColumnName("rcopia_patient_id");
            entity.Property(e => e.RcopiaXml)
                .HasColumnType("xml")
                .HasColumnName("rcopia_xml");
            entity.Property(e => e.ReferralDetails)
                .HasMaxLength(255)
                .HasColumnName("referral_details");
            entity.Property(e => e.ReferralSourceId).HasColumnName("referral_source_id");
            entity.Property(e => e.ReferringOrganizationId).HasColumnName("referring_organization_id");
            entity.Property(e => e.ReferringPhysicianId).HasColumnName("referring_physician_id");
            entity.Property(e => e.ReligionId).HasColumnName("religion_id");
            entity.Property(e => e.RequiresInterpreter).HasColumnName("requires_interpreter");
            entity.Property(e => e.SalutationId).HasColumnName("salutation_id");
            entity.Property(e => e.SecondaryAddressLine1)
                .HasMaxLength(100)
                .HasColumnName("secondary_address_line_1");
            entity.Property(e => e.SecondaryAddressLine2)
                .HasMaxLength(100)
                .HasColumnName("secondary_address_line_2");
            entity.Property(e => e.SecondaryCity)
                .HasMaxLength(25)
                .HasColumnName("secondary_city");
            entity.Property(e => e.SecondaryStateCode)
                .HasMaxLength(2)
                .HasColumnName("secondary_state_code");
            entity.Property(e => e.SecondaryZipCode)
                .HasMaxLength(5)
                .HasColumnName("secondary_zip_code");
            entity.Property(e => e.SecondaryZipPlus4)
                .HasMaxLength(4)
                .HasColumnName("secondary_zip_plus_4");
            entity.Property(e => e.Sex).HasColumnName("sex");
            entity.Property(e => e.SignatureOnFile).HasColumnName("signature_on_file");
            entity.Property(e => e.SofDate)
                .HasColumnType("datetime")
                .HasColumnName("SOF_date");
            entity.Property(e => e.Ssn)
                .HasMaxLength(11)
                .HasColumnName("ssn");
            entity.Property(e => e.SummaryDocument)
                .HasColumnType("image")
                .HasColumnName("summary_document");
            entity.Property(e => e.UniqueNumber)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.UserId).HasColumnName("user_id");
            entity.Property(e => e.WorkPhone)
                .HasMaxLength(14)
                .HasColumnName("work_phone");
            entity.Property(e => e.WorkPhoneExt)
                .HasMaxLength(6)
                .HasColumnName("work_phone_ext");

            entity.HasOne(d => d.DefaultFacilityNavigation).WithMany(p => p.Patients)
                .HasForeignKey(d => d.DefaultFacility)
                .HasConstraintName("FK_patient_facility");
        });
        modelBuilder.HasSequence("hibernate_sequence");

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
