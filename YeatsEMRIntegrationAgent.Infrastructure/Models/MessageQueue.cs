﻿using System;
using System.Collections.Generic;

namespace YeatsEMRIntegrationAgent.Infrastructure.Models;

public partial class MessageQueue
{
    public long Id { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? Type { get; set; }

    public DateTime? ProcessingDate { get; set; }

    public string? Payload { get; set; }

    public string? Error { get; set; }

    public string? ResourceType { get; set; }

    public string? Source { get; set; }

    public string? Status { get; set; }
}
