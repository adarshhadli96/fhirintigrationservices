﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace YeatsEMRIntegrationAgent.Infrastructure.Models;

public partial class IntegrationsContext : DbContext
{
    public IntegrationsContext()
    {
    }

    public IntegrationsContext(DbContextOptions<IntegrationsContext> options)
        : base(options)
    {
    }

    public virtual DbSet<MessageQueue> MessageQueues { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=174.73.51.169\\\\\\\\SQLEXPRESS,6099;Database=Integrations;Trusted_Connection=False;TrustServerCertificate=True;User ID=sa;Password=Matrics@1234;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<MessageQueue>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__MESSAGE___3214EC0786A56808");

            entity.ToTable("MESSAGE_QUEUE");

            entity.Property(e => e.ResourceType).HasMaxLength(50);
            entity.Property(e => e.Source).HasMaxLength(50);
            entity.Property(e => e.Status).HasMaxLength(10);
        });
        modelBuilder.HasSequence("hibernate_sequence");

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
