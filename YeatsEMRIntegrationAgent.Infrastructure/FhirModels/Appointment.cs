﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Infrastructure.FhirModels
{
    public class Appointment
    {
        public string Identifier { get; set; }
        public string Status { get; set; }
        public string CancelationReason { get; set; }
        public string ServiceCategory { get; set; }
        public string ServiceType { get; set; }
        public string Speciality { get; set; }
        public string AppointmentType { get; set; }
        public string ResoneCode { get; set; }
        public string Priority { get; set; }
        public string Description { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string Created { get; set; }
        public string Comment { get; set; }
        public string PatientInstruction { get; set; }
        public List<Participant> Participant { get; set; }

    }

    public class Participant
    {
        public string Type { get; set; }
        public string Actor { get; set; }
        public string Required { get; set; }
        public string ParticipantStatus { get; set; }

    }
}
