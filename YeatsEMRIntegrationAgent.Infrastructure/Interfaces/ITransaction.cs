﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Core.Entities;

namespace YeatsEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface ITransaction
    {
        public bool insert(Transaction transaction);
        public bool update(Transaction transaction);
        public List<Transaction> GetAll();
        public bool delete(Transaction transaction);
    }
}
