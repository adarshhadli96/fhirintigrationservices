﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Core.Entities;

namespace YeatsEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IInsurance
    {
        public bool insert(Insurance insurance);
        public bool update(Insurance insurance);
        public List<Insurance> GetAll();
        public bool delete(Insurance insurance);
    }
}
