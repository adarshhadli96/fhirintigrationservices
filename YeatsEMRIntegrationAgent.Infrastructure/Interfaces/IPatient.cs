﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Infrastructure.Models;

namespace YeatsEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IPatient
    {
        public bool insert(Patient patient);
        public bool update(Patient patient);
        public List<Patient> GetAll();
        public bool delete(int id);
    }
}
