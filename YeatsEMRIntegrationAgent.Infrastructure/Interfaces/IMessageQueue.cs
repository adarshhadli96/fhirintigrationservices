﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Infrastructure.DTO;


namespace YeatsEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IMessageQueue
    {
        public bool insert(Core.Entities.MessageQueue imq);
        public List<Core.Entities.MessageQueue> GetAll();
        
    }
}
