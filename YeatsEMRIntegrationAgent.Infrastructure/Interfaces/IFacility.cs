﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Infrastructure.Models;

namespace YeatsEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IFacility
    {
        public bool insert(Facility facility);
        public bool update(Facility facility);
        public List<Facility> GetAll();
        public bool delete(int id);
    }
}
