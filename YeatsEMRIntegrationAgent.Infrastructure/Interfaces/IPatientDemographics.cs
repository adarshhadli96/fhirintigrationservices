﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Core.Entities;

namespace YeatsEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IPatientDemographics
    {
        public bool insert(PatientDemographics patientDemographics);
        public bool update(PatientDemographics patientDemographics);
        public List<PatientDemographics> GetAll();
        public bool delete(PatientDemographics patientDemographics);
    }
}
