﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Infrastructure.Models;

namespace YeatsEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IAppointment
    {
        public bool insert(Appointment appointment);
        public bool update(Appointment appointment);
        public List<Appointment> GetAll();
        public bool delete(int id);
    }
}
