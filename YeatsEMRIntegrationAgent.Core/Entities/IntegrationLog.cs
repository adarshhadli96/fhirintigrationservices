﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Core.Entities
{
    public class IntegrationLog
    {
        public string Id { get; set; }
        public string Direction  { get; set; }
        public string Status { get; set; }
        public string Data { get; set; }
        public string Event { get; set; }
        public string Notes { get; set; }
        public string InternalId { get; set; }
        public string ExternalId { get; set; }
        public string AckStatus { get; set; }
        public string AckNotes { get; set; }
        public string ParentId { get; set; }
        public string ExtMsgId { get; set; }
    }
}
