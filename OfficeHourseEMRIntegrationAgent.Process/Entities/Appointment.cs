﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHourseEMRIntegrationAgent.Process.Entities
{
    public class Appointment
    {
        public string AppointmentId { get; set; }
        public string AppointmentDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Duration { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string Type { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string Notes { get; set; }
        public string CaseNumber { get; set; }

        public Patient Patient { get; set; }
        public Facility Facility { get; set; }

    }
}
