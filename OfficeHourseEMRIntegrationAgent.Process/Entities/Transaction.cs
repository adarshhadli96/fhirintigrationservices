﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace YeatsEMRIntegrationAgent.Process.Entities
{
    
    public class Transaction
    {
     public string BillTransactionId { get; set; }
     public string BillHeaderId { get; set; }
     public string SuperBillId { get; set; }
     public string PatientId { get; set; }
     public string VisitId { get; set; }
     public string AppointmentId { get; set; }
     public string MrPatientEncounterId { get; set; }
     public string ChartNumber { get; set; }
     public string ServiceStartDate { get; set; }
     public string ServiceEndDate { get; set; }
     public string BillTransactionReference { get; set; }
     public string TransactionCodeId { get; set; }
     public string PlaceOfServiceCode { get; set; }
     public string CptCode { get; set; }
     public string TypeOfServiceCode { get; set; }
     public string Units { get; set; }
     public string Diagnosis1 { get; set; }
     public string Diagnosis2 { get; set; }
     public string Diagnosis3 { get; set; }
     public string Diagnosis4 { get; set; }
     public string Diagnosis5 { get; set; }
     public string Diagnosis6 { get; set; }
     public string Diagnosis7 { get; set; }
     public string Diagnosis8 { get; set; }
     public string Modifier1 { get; set; }
     public string Modifier2 { get; set; }
     public string Modifier3 { get; set; }
     public string Modifier4 { get; set; }
     public string RvuValue { get; set; }
     public string ActualCharge { get; set; }
     public string Extended { get; set; }
     public string PatientPortion { get; set; }
     public string Taxable { get; set; }
     public string ChargePatientOnly { get; set; }
     public string DetailNote { get; set; }
     public string PrintDetailNoteOnBillStatement { get; set; }
     public string InsuranceNote { get; set; }
     public string PrintInsuranceNoteOnBillStatement { get; set; }
     public string Posted { get; set; }
     public string DatePosted { get; set; }
     public string PrimaryClaimId { get; set; }
     public string SecondaryClaimId { get; set; }
     public string TertiaryClaimId { get; set; }
     public string EPSDTReferralCode1 { get; set; }
     public string EPSDTReferralCode2 { get; set; }
     public string EPSDTReferralCode3 { get; set; }
     public string DateCreated { get; set; }
     public string CreatedByUserId { get; set; }
     public string DateLastUpdated { get; set; }
     public string LastUpdatedByUserId { get; set; }
     public string InsurancePriorAuthorizationNumber { get; set; }
     public string PrimaryInsuranceAmountReceived { get; set; }
     public string SecondaryInsuranceAmountReceived { get; set; }
     public string TertiaryInsuranceAmountReceived { get; set; }
     public string PatientAmountReceived { get; set; }
     public string AdjustmentAmount { get; set; }
     public string WriteOffAmount { get; set; }
     public string ModifyUnitCharge { get; set; }
     public string MrPatientEncounterCptDiagnosisAssociationId { get; set; }
     public string PrimaryInsuranceChargesAllowed { get; set; }
     public string SecondaryInsuranceChargesAllowed { get; set; }
     public string TertiaryInsuranceChargesAllowed { get; set; }
     public string PrimaryCopayAmountRequired { get; set; }
     public string PrimaryCopayAmountPaid { get; set; }
     public string PrimaryCoInsuranceAmountRequired { get; set; }
     public string PrimaryCoInsuranceAmountPaid { get; set; }
     public string PrimaryDeductibleAmountRequired { get; set; }
     public string PrimaryDeductibleAmountPaid { get; set; }
     public string SecondaryCopayAmountRequired { get; set; }
     public string SecondaryCopayAmountPaid { get; set; }
     public string SecondaryCoInsuranceAmountRequired { get; set; }
     public string SecondaryCoInsuranceAmountPaid { get; set; }
     public string SecondaryDeductibleAmountRequired { get; set; }
     public string SecondaryDeductibleAmountPaid { get; set; }
     public string TertiaryCopayAmountRequired { get; set; }
     public string TertiaryCopayAmountPaid { get; set; }
     public string TertiaryCoInsuranceAmountRequired { get; set; }
     public string TertiaryCoInsuranceAmountPaid { get; set; }
     public string TertiaryDeductibleAmountRequired { get; set; }
     public string TertiaryDeductibleAmountPaid { get; set; }
     public string Status { get; set; }
     public string ClaimRemittanceAdviceServicePaymentId { get; set; }
     public string SelectedPointer { get; set; }
     public string BillersNote { get; set; }

    }
}
