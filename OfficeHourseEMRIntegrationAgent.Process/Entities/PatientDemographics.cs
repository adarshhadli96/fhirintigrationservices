﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace YeatsEMRIntegrationAgent.Process.Entities
{
    public class PatientDemographics
    {
        public Patient Patient { get; set; }
        public Patient Guarantor { get; set; }
        public string GuarantorRelationShip { get; set; }
        public List<Insurance> InsuranceList { get; set; }
        public Facility DefaultFacility { get; set; }
        public List<Physician> Physicians { get; set; }
    }
}
