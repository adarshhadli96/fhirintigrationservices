﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Process.Entities
{
    public class Patient
    {
        public string PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string Language { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string WPhone { get; set; }
        public string HPhone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string AltPhone { get; set; }
        public string Ssn { get; set; }
        public string Sex { get; set; }
        public string Dob { get; set; }
        public string DefaultPhysician { get; set; }
        public string Email { get; set; }
        public string MarritalStatus { get; set; }
        public string Inactive { get; set; }
        public string ChartNumber { get; set; }
    }
}
