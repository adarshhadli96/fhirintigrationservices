using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Net.Http;

namespace OfficeHourseEMRIntegrationAgent.Process
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IHost host = Host.CreateDefaultBuilder(args)
                .ConfigureServices(services =>
                {
                    services.AddHostedService<Worker>();
                    services.AddScoped<HttpClient>();
                })
                .Build();

            host.Run();
        }
    }
}