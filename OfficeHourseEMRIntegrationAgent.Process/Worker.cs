using Newtonsoft.Json;
using OfficeHoursEMRIntegrationAgent.Application.DTOs;
using OfficeHoursEMRIntegrationAgent.Application.Models;
using System.Net.Http;
using System.Text;
using System.Net.Http.Json;

namespace OfficeHourseEMRIntegrationAgent.Process
{
    public class Worker : BackgroundService
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<Worker> _logger;

        public Worker(HttpClient httpClient, ILogger<Worker> logger)
        {
            _logger= logger;
            _httpClient = httpClient;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //await GetAsync();
            await PostAsync();
            _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
            await Task.Delay(1000, stoppingToken);
        }

        private async Task GetAsync()
        {
            var response = await _httpClient.GetAsync("https://yeatspatientportal-integration-staging.azurewebsites.net/MessageQueue/pull?sourceSystem=Medisoft");

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<List<MessageQueue>>(json);

                // Mapping the external data to Application layer "MessageQueue" objects
                var messageQueueFromApi = data.Select(d => new MessageQueue
                {
                    Id = d.Id,
                    CreatedDate = d.CreatedDate,
                    Type = d.Type,
                    ProcessingDate = d.ProcessingDate,
                    Payload = d.Payload,
                    Error = d.Error,
                    ResourceType = d.ResourceType,
                    Source = d.Source,
                    Status = d.Status
                }).ToList();
                Console.WriteLine($"Messages fetched from the api is: {data.Count}");
                Console.WriteLine($"Messages mapped to Application layer is: {messageQueueFromApi.Count}");
            }
            else
            {
                Console.WriteLine($"Failed to fetch data from external API. Status code: {response.StatusCode}");
            }
        }

        private MessageQueue DataSeedToMessageQueue()
        {
            List<PatientDto> patients = new List<PatientDto>();
            patients.Add(
                new PatientDto
                {
                    Identifier = "PAT0099",
                    Prefix = "Mr",
                    FirstName = "New Xyz",
                    LastName = "New LN",
                    BirthDate = "1999-12-31",
                    BirthPlace = "new Bplace",
                    Gender = "Male",
                    CitizenshipCode = "12345",
                    Address = new AddressDto { StreetNo = "#1", StreetName = "New Street", AppartmentNo = "1234", City = "My", Country = "US", PostalCode = "560345", Type = "" }
                });

            MessageQueue messageQueue = new MessageQueue();
            messageQueue.CreatedDate = String.Format("{0:yyyy-MM-dd}", DateTime.Now);
            messageQueue.Id = "";
            messageQueue.ProcessingDate = "";
            messageQueue.Payload = JsonConvert.SerializeObject(patients);
            messageQueue.Source = "Medisoft";
            messageQueue.Status = "";
            messageQueue.ResourceType = "Patient";
            messageQueue.Type = "I";

            return messageQueue;
        }
        private async Task PostAsync()
        {   ////////////////////////////////////////////////////////////////////////////////////////////////////

            var messageQueue = JsonConvert.SerializeObject(DataSeedToMessageQueue());

            var content = new StringContent(messageQueue, Encoding.UTF8, "application/json");

            var request = await _httpClient.PostAsync("https://yeatspatientportal-integration-staging.azurewebsites.net/MessageQueue/push", content);

            if (request.IsSuccessStatusCode)
            {
                Console.WriteLine($"Data pushed successfully to external API. Status code: {request.StatusCode}");
            }
            else
            {
                Console.WriteLine($"Failed to push data to external API. Status code: {request.StatusCode}");
            }
        }
    }
}