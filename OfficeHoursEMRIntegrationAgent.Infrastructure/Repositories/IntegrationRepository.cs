﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Repositories
{
    public class IntegrationRepository : IIntegration
    {
        public void Delete(Integration integration)
        {
            throw new NotImplementedException();
        }

        public List<Integration> GetAll()
        {
            throw new NotImplementedException();
        }

        public Integration GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Insert(Integration integration)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Integration integration)
        {
            throw new NotImplementedException();
        }
    }
}
