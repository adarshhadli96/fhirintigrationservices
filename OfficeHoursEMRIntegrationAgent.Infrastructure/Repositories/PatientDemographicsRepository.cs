﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Repositories
{
    public class PatientDemographicsRepository : IPatientDemographics
    {
        public void Delete(PatientDemographics patientDemographics)
        {
            throw new NotImplementedException();
        }

        public List<PatientDemographics> GetAll()
        {
            throw new NotImplementedException();
        }

        public PatientDemographics GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Insert(PatientDemographics patientDemographics)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(PatientDemographics patientDemographics)
        {
            throw new NotImplementedException();
        }
    }
}
