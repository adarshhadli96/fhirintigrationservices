﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Repositories
{
    public class PhysicianRepository : IPhysician
    {
        public void Delete(Physician physician)
        {
            throw new NotImplementedException();
        }

        public List<Physician> GetAll()
        {
            throw new NotImplementedException();
        }

        public Physician GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Insert(Physician physician)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Physician physician)
        {
            throw new NotImplementedException();
        }
    }
}
