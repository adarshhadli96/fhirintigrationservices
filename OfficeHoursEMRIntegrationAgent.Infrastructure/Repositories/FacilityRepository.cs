﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Repositories
{
    public class FacilityRepository : IFacility
    {
        public void Delete(Facility facility)
        {
            throw new NotImplementedException();
        }

        public List<Facility> GetAll()
        {
            throw new NotImplementedException();
        }

        public Facility GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Insert(Facility facility)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Facility facility)
        {
            throw new NotImplementedException();
        }
    }
}
