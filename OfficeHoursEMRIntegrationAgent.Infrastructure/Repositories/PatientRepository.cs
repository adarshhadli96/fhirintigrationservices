﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Repositories
{
    public class PatientRepository : IPatient
    {
        public void Delete(Patient patient)
        {
            throw new NotImplementedException();
        }

        public List<Patient> GetAll()
        {
            throw new NotImplementedException();
        }

        public Patient GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Insert(Patient patient)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Patient patient)
        {
            throw new NotImplementedException();
        }
    }
}
