﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Repositories
{
    public class InsuranceRepository : IInsurance
    {
        public void Delete(Insurance insurance)
        {
            throw new NotImplementedException();
        }

        public List<Insurance> GetAll()
        {
            throw new NotImplementedException();
        }

        public Insurance GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Insert(Insurance insurance)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Insurance insurance)
        {
            throw new NotImplementedException();
        }
    }
}
