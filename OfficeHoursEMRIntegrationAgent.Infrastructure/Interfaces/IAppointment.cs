﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IAppointment
    {
        List<Appointment> GetAll();
        Appointment GetById(string id);
        void Insert(Appointment appointment);
        void Update(Appointment appointment);
        void Delete(Appointment appointment);
        void Save();
    }
}
