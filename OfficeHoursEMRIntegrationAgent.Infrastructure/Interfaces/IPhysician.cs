﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IPhysician
    {
        List<Physician> GetAll();
        Physician GetById(string id);
        void Insert(Physician physician);
        void Update(Physician physician);
        void Delete(Physician physician);
        void Save();
    }
}
