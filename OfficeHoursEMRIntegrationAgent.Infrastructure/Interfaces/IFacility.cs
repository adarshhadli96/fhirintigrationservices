﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IFacility
    {
        List<Facility> GetAll();
        Facility GetById(string id);
        void Insert(Facility facility);
        void Update(Facility facility);
        void Delete(Facility facility);
        void Save();
    }
}
