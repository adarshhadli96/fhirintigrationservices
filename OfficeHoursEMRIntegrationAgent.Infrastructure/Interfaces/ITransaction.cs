﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface ITransaction
    {
        List<Transaction> GetAll();
        Transaction GetById(string id);
        void Insert(Transaction transaction);
        void Update(Transaction transaction);
        void Delete(Transaction transaction);
        void Save();
    }
}
