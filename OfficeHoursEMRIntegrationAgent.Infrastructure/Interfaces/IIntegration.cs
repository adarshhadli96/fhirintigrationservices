﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IIntegration
    {
        List<Integration> GetAll();
        Integration GetById(string id);
        void Insert(Integration integration);
        void Update(Integration integration);
        void Delete(Integration integration);
        void Save();

    }
}
