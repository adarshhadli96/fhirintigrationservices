﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IPatientDemographics
    {
        List<PatientDemographics> GetAll();
        PatientDemographics GetById(string id);
        void Insert(PatientDemographics patientDemographics);
        void Update(PatientDemographics patientDemographics);
        void Delete(PatientDemographics patientDemographics);
        void Save();
    }
}
