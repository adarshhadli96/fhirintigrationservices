﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IPatient
    {
        List<Patient> GetAll();
        Patient GetById(string id);
        void Insert(Patient patient);
        void Update(Patient patient);
        void Delete(Patient patient);
        void Save();
    }
}
