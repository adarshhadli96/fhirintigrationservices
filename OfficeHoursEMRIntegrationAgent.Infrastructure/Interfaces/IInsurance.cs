﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IInsurance
    {
        List<Insurance> GetAll();
        Insurance GetById(string id);
        void Insert(Insurance insurance);
        void Update(Insurance insurance);
        void Delete(Insurance insurance);
        void Save();
    }
}
