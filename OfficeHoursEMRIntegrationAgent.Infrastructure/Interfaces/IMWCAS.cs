﻿using OfficeHoursEMRIntegrationAgent.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Infrastructure.Interfaces
{
    public interface IMWCAS
    {
        List<MWCAS> GetAll();
        MWCAS GetById(string id);
        void Insert(MWCAS mwcas);
        void Update(MWCAS mwcas);
        void Delete(MWCAS mwcas);
        void Save();
    }
}
