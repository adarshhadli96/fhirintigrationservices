﻿using Newtonsoft.Json;
using OfficeHoursEMRIntegrationAgent.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Application.DTOs
{
    [Serializable]
    public class PatientDto : BasePatient
    {
        [JsonProperty("address")]
        public AddressDto Address { get; set; }
    }
}
