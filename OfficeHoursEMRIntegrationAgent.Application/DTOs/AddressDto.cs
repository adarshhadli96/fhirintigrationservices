﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Application.DTOs
{
    [Serializable]
    public class AddressDto
    {
        [JsonProperty("streetName")]
        public string StreetName { get; set; }

        [JsonProperty("streetNo")]
        public string StreetNo { get; set; }

        [JsonProperty("appartmentNo")]
        public string AppartmentNo { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
