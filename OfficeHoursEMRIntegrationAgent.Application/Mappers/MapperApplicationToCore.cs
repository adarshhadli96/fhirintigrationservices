﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MessageQueue = OfficeHoursEMRIntegrationAgent.Core.Models.MessageQueue;

namespace OfficeHoursEMRIntegrationAgent.Application.Mappers
{
    public class MapperApplicationToCore
    {    
        public static OfficeHoursEMRIntegrationAgent.Core.Models.MessageQueue MapApplicationToCore(OfficeHoursEMRIntegrationAgent.Application.Models.MessageQueue mapmessageQueue)
        {
            var message = new OfficeHoursEMRIntegrationAgent.Core.Models.MessageQueue
            {
                Id = mapmessageQueue.Id,
                CreatedDate = mapmessageQueue.CreatedDate,
                Type = mapmessageQueue.Type,
                ProcessingDate = mapmessageQueue.ProcessingDate,
                Payload = mapmessageQueue.Payload,
                Error = mapmessageQueue.Error,
                ResourceType = mapmessageQueue.ResourceType,
                Source = mapmessageQueue.Source,
                Status = mapmessageQueue.Status
            };
            return message;
        }

        public static List<OfficeHoursEMRIntegrationAgent.Application.Models.MessageQueue> MapApplicationToCore(List<OfficeHoursEMRIntegrationAgent.Core.Models.MessageQueue> mapmessageQueues)
        {
            var listmessages = mapmessageQueues.Select(x => new OfficeHoursEMRIntegrationAgent.Application.Models.MessageQueue
            {
                Id = x.Id,
                CreatedDate = x.CreatedDate,
                Type = x.Type,
                ProcessingDate = x.ProcessingDate,
                Payload = x.Payload,
                Error = x.Error,
                ResourceType = x.ResourceType,
                Source = x.Source,
                Status = x.Status
            }).ToList();
            return listmessages;
        }
    }
}
