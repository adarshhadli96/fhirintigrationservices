﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Application.Models
{
    public class BasePatient
    {
        public string Identifier { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string CitizenshipCode { get; set; }
        public string Gender { get; set; }
        public override string ToString()
        {
            return $"{Identifier} {FirstName} {LastName}";
        }
    }
}
