﻿using OfficeHoursEMRIntegrationAgent.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Application.Interfaces
{
    public interface IMessageQueue
    {
        public bool insert(MessageQueue imq);

        public List<MessageQueue> GetAllMessageQueues();
    }
}
