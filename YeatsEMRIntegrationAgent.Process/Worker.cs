using Azure;
using Newtonsoft.Json;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using YeatsEMRIntegrationAgent.Application.DTOs;
using MessageQueue = YeatsEMRIntegrationAgent.Application.Models.MessageQueue;

namespace YeatsEMRIntegrationAgent.Process
{
    public class Worker : BackgroundService
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<Worker> _logger;

        public Worker(HttpClient httpClient, ILogger<Worker> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                //await GetAsync();
                await PostAsync();

                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(1000, stoppingToken);
            }
        }

        private async Task GetAsync()
        {
            var response = await _httpClient.GetAsync("https://yeatspatientportal-integration-staging.azurewebsites.net/MessageQueue/pull?sourceSystem=Medisoft");

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<List<MessageQueue>>(json);

                // Mapping the external data to Application layer "MessageQueue" objects
                var messageQueueFromApi = data.Select(d => new MessageQueue
                {
                    Id = d.Id,
                    CreatedDate = d.CreatedDate,
                    Type = d.Type,
                    ProcessingDate = d.ProcessingDate,
                    Payload = d.Payload,
                    Error = d.Error,
                    ResourceType = d.ResourceType,
                    Source = d.Source,
                    Status = d.Status
                }).ToList();
                Console.WriteLine($"Messages fetched from the api is: {data.Count}");
                Console.WriteLine($"Messages mapped to Application layer is: {messageQueueFromApi.Count}");
            }
            else
            {
                Console.WriteLine($"Failed to fetch data from external API. Status code: {response.StatusCode}");
            }
        }
        private async Task PostAsync()
        {
            var address = YeatsEMRIntegrationAgent.Dummy.CodeRefactored.DataSource.TrialData.GetAddresses();

            var adressdata = JsonConvert.SerializeObject(address.Select(a => new Dummy.CodeRefactored.Models.Address
            {
                StreetName = a.StreetName,
                StreetNo = a.StreetNo,
                AppartmentNo = a.AppartmentNo,
                PostalCode = a.PostalCode,
                City = a.City,
                Country = a.Country,
                Type = a.Type,
            }));

            //////////////////////////////////////////////////////////////////////////////////////////////////////////

            var patient = YeatsEMRIntegrationAgent.Dummy.CodeRefactored.DataSource.TrialData.GetPatients();

            var patientdata = JsonConvert.SerializeObject(patient.Select(p => new Dummy.CodeRefactored.Models.Patient
            {
                Identifier = p.Identifier,
                Prefix = p.Prefix,
                FirstName = p.FirstName,
                LastName = p.LastName,
                BirthDate = p.BirthDate,
                BirthPlace = p.BirthPlace,
                CitizenshipCode = p.CitizenshipCode,
                Gender = p.Gender,
                Address = p.Address
            }));

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            var messageQueues = YeatsEMRIntegrationAgent.Dummy.CodeRefactored.DataSource.TrialData.GetMessageQueues();

            
            string strJson = JsonConvert.SerializeObject(messageQueues);
            var content = new StringContent(strJson, Encoding.UTF8, "application/json");

            

            var request = await _httpClient.PostAsJsonAsync("https://yeatspatientportal-integration-staging.azurewebsites.net/MessageQueue/push", content);

            if (request.IsSuccessStatusCode)
            {
                Console.WriteLine($"Data pushed successfully to external API. Status code: {request.StatusCode}");
            }
            else
            {
                Console.WriteLine($"Failed to push data to external API. Status code: {request.StatusCode}");
            }
        }
    }
}