using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using YeatsEMRIntegrationAgent.Application.Models;
using YeatsEMRIntegrationAgent.Application.Services;
using YeatsEMRIntegrationAgent.Infrastructure.DTO;
using YeatsEMRIntegrationAgent.Infrastructure.Models;

namespace YeatsEMRIntegrationAgent.Process
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IHost host = Host.CreateDefaultBuilder(args)
                .ConfigureServices(services =>
                {
                    services.AddHttpClient();
                    services.AddHostedService<Worker>();
                })
                .Build();
          
            host.Run();
        }
    }
}