﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Core.Entities
{
    public class Physician
    {
        public string PhysicianId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string HPhone { get; set; }
        public string Fax { get; set; }
        public string Ssn { get; set; }
        public string License { get; set; }
        public string DeaNumber { get; set; }
        public string Email { get; set; }
        public string Npi { get; set; }
        public string LicenseState { get; set; }
        public string TaxIdentifier { get; set; }
        public string Upin { get; set; }
        public string ProviderCode { get; set; }
        public string MedicareParticipant { get; set; }
        public string TaxonomyCode { get; set; }
        public string WPhone { get; set; }
        public string Mobile { get; set; }
        public string LoginName { get; set; }
        public string Speciality { get; set; }
        public string Type { get; set; }
    }
}
