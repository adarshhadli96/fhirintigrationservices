﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Core.Entities
{
    public class Insurance
    {
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAddress1 { get; set; }
        public string ProviderAddress2 { get; set; }
        public string ProviderCity { get; set; }
        public string ProviderState { get; set; }
        public string ProviderZip { get; set; }
        public string ProviderPhone { get; set; }
        public string InsuredPerson { get; set; }
        public string PolicyNumber { get; set; }
        public string GroupNumber { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Relationship { get; set; }
        public Patient InsuredPersoninfo { get; set; }
        public string Order { get; set; }
    }
}
