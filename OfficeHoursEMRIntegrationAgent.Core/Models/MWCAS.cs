﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Core.Entities
{
    public class MWCAS
    {
        public string GuarantorId { get; set; }
        public string RefferingPhysicianId { get; set; }
        public string DefaultPhysicianId { get; set; }
        public string FacilityId { get; set; }
        public string MarritalStatus { get; set; }
        public List<Insurance> InsuranceList { get; set; }
    }
}
