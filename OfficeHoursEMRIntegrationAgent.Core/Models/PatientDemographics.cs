﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Core.Entities
{
    public class PatientDemographics
    {
        public Patient Patient { get; set; }
        public Patient Guarantor { get; set; }
        public string GuarantorRelationShip { get; set; }
        public Facility DefaultFacility { get; set; }
        public List<Physician> Physicians { get; set; }
        public Physician Physician { get; set; }
    }
}
