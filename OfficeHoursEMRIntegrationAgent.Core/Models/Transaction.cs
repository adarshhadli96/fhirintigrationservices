﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Core.Entities
{
    public class Transaction
    {
       public string Id { get; set; }
       public string Post { get; set; }
       public string TransactionStatus { get; set; }
       public string ChartNumber { get; set; }
       public string CaseNumber { get; set; }
       public string Provider { get; set; }
       public string DateFrom { get; set; }
       public string DiagnosisCode1 { get; set; }
       public string DiagnosisCode2 { get; set; }
       public string DiagnosisCode3 { get; set; }
       public string DiagnosisCode4 { get; set; }
       public string ProcedureCode { get; set; }
       public string Modifier1 { get; set; }
       public string Modifier2 { get; set; }
       public string Modifier3 { get; set; }
       public string Modifier4 { get; set; }
       public string Units { get; set; }
       public string ProcedureOrder { get; set; }
       public string PatientLastName { get; set; }
       public string PatientFirstName { get; set; }
       public string BirthDate { get; set; }
       public string Ssn { get; set; }
       public string Exception { get; set; }
       public string FacilityCode { get; set; }
       public string CreatingApp { get; set; }
       public string Amount { get; set; }
       public string HL7MessageId { get; set; }
       public string ErrorMsg { get; set; }
       public string DateCreated { get; set; }
       public string DateModified { get; set; }
       public string Viewed { get; set; } 
       public string ClaimStatus { get; set; }
       public string Checksum { get; set; }
       public string DiagnosisCode5 { get; set; }
       public string DiagnosisCode6 { get; set; } 
       public string DiagnosisCode7 { get; set; }
       public string DiagnosisCode8 { get; set; }
       public string DiagnosisCode9 { get; set; }
       public string DiagnosisCode10 { get; set; }
       public string DiagnosisCode11 { get; set; }
       public string DiagnosisCode12 { get; set; }
       public string ImmunizationBatchNumber { get; set; }
       public string MobileNotes { get; set; }

       public Physician Physician { get; set;}
       public Facility Facility { get; set; }
    }
}
