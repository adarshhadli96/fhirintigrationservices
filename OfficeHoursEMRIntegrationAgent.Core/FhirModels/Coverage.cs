﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Core.FhirModels
{
    public class Coverage
    {
        public string Identifier { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string PolicyHolder { get; set; }
        public string Subscriber { get; set; }
        public string SubscriberId { get; set; }
        public string Beneficiary { get; set; }
        public string Dependent { get; set; }
        public string Relationship { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        public string Payor { get; set; }
        public string Order { get; set; }
        public string Network { get; set; }
        public string Subrogation { get; set; }
        public string Contract { get; set; }
        
        public List<Class> Class { get; set; }

        public List<CostToBeneficiary> CostToBeneficiary { get; set; }

    }



    public class Class
    {
        public string Type { get; set; }
        public string Value { get; set; }
        public string Name { get; set; }
    }

    public class CostToBeneficiary
    {
        public string Type { get; set; }
        public string ValueQuentity { get; set; }
        public string ValueMoney { get; set; }
        public List<Exception> Exception { get; set; }
    }

    public class Exception
    {
        public string Type { get; set; }
        public string Period { get; set; }
    }

}
