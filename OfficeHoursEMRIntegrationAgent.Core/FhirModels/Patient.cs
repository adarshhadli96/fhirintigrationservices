﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeHoursEMRIntegrationAgent.Core.FhirModels
{
    public class Patient
    {
        public string Identifier { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string CitizenshipCode { get; set; }
        public string Gender { get; set; }
        public Address Address { get; set; }

    }

    public class Address
    {
        public string StreetName { get; set; }
        public string StreetNo { get; set; }
        public string AppartmentNo { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Type { get; set; }

    }
}
