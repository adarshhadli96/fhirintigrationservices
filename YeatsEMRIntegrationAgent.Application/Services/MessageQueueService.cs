﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Application.DTOs;
using YeatsEMRIntegrationAgent.Application.Interfaces;
using YeatsEMRIntegrationAgent.Application.Mappers;
using YeatsEMRIntegrationAgent.Application.Models;
using YeatsEMRIntegrationAgent.Infrastructure.Repositories;

namespace YeatsEMRIntegrationAgent.Application.Services
{
    public class MessageQueueService : IMessageQueue
    {
        public List<MessageQueue> GetAll()
        {
            MessageQueueRepository messageQueueRepository = new MessageQueueRepository();

            return MapperApplicationToCore.MapApplicationToCore(messageQueueRepository.GetAll());
        }


        public bool insert(MessageQueue imq)
        {
            MessageQueueRepository messageQueueRepository= new MessageQueueRepository();
            var mappedMessages = MapperApplicationToCore.MapApplicationToCore(imq);
            bool returnValue = messageQueueRepository.insert(mappedMessages);
            return returnValue;
        }
    }
}
