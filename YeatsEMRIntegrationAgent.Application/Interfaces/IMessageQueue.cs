﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YeatsEMRIntegrationAgent.Application.Models;

namespace YeatsEMRIntegrationAgent.Application.Interfaces
{
    public interface IMessageQueue
    {
        public bool insert(MessageQueue imq);

        public List<Models.MessageQueue> GetAll();

    }
}
