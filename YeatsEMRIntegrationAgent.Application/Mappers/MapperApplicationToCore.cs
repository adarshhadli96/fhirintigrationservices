﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YeatsEMRIntegrationAgent.Application.Mappers
{
    public class MapperApplicationToCore
    {    
        public static YeatsEMRIntegrationAgent.Core.Entities.MessageQueue MapApplicationToCore(YeatsEMRIntegrationAgent.Application.Models.MessageQueue mapmessageQueue)
        {
            var message = new YeatsEMRIntegrationAgent.Core.Entities.MessageQueue
            {
                Id = mapmessageQueue.Id,
                CreatedDate = mapmessageQueue.CreatedDate,
                Type = mapmessageQueue.Type,
                ProcessingDate = mapmessageQueue.ProcessingDate,
                Payload = mapmessageQueue.Payload,
                Error = mapmessageQueue.Error,
                ResourceType = mapmessageQueue.ResourceType,
                Source = mapmessageQueue.Source,
                Status = mapmessageQueue.Status
            };
            return message;
        }

        public static List<YeatsEMRIntegrationAgent.Application.Models.MessageQueue> MapApplicationToCore(List<YeatsEMRIntegrationAgent.Core.Entities.MessageQueue> mapmessageQueues)
        {
            var listmessages = mapmessageQueues.Select(x => new YeatsEMRIntegrationAgent.Application.Models.MessageQueue
            {
                Id = x.Id,
                CreatedDate = x.CreatedDate,
                Type = x.Type,
                ProcessingDate = x.ProcessingDate,
                Payload = x.Payload,
                Error = x.Error,
                ResourceType = x.ResourceType,
                Source = x.Source,
                Status = x.Status
            }).ToList();
            return listmessages;
        }
    }
}
